//No hotel morte lenta, estavam hospedados 5 hospédes, Renato Faca, Maria Cálice, Roberto Arma, Roberta Corda e Uesllei.
//A meia noite, o hóspede do quarto 101 foi encontrado morto e os 5 suspeitos são os citados acima.
//Descubra quem é o assassino sabendo que:
//Se o nome do suspeito contem 3 vogais ou mais(Nome completo) e nenhuma dessas vogais é u ou o, ele pode ser o assassino.
//Se o suspeito for do sexo feminino, só sera assassina se tiver utilizado uma arma branca.
//Se o suspeito for do sexo masculino, ele deveria estar no saguão do hotel a 00h30 para ser considerado o assasino.
//Sabendo dessas informações acima, projete o algorítmo que fornecido o nome do suspeito, indique se ele pode ou não ser o assassino.

package main

import (
	"fmt"
	"strings"
)

func main() {
	// solcitar nome ao usuário
	name := askString("Nome: ")

	// verifica se o nome é uma string
	vn := checkString(name, "1234567890!@#$%*()<>.,:;?/")

	// solcitar nome ao usuário

	sobrenome := askString("Sobrenome: ")

	// verifica se o nome é uma string
	vs := checkString(sobrenome, "1234567890!@#$%*()<>.,:;?/")

	// solcitar genero ao user
	gender := askString("Genero F/M:")

	//solicita horario
	time := askString("horario(hh.mm):")

	//verifica se o sobrenome ta correto

	if !(vn && vs) {
		fmt.Println("nao sabe nem oq e um nome")
	}

	// verifica se o genero esta correto
	if !(gender == "F" || gender == "M") {
		fmt.Println("entre com algum genero valido (F/M)")
	}

	// verifica se o nome contains as vogais corretas
	vv := validadeVogals(name)

	if !vv {
		fmt.Println("nao e suspeito")
	}
	weapon := validadeWeapon(sobrenome)
	//verifica assassina
	vw := validadeassassin(getgender(gender), weapon)
	//vaerifica assassina
	vm := validadeassassin(!(getgender(gender)), checktime(time))

	if !(vm || vw) {
		fmt.Print("nao serve nem pra matar alguem")
	}

	fmt.Println("ASSASSINOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO")

	//verifica se o sobrenome e composto por alguma arma branca

}
func checktime(time string) bool {
	if time == "00:30" {
		return true
	}
	return false
}

func getgender(gender string) bool {
	if gender == "F" {
		return true
	}
	return false
}

func validadeassassin(x bool, y bool) bool {
	if x && y {
		return true
	}
	return false
}

func validadeVogals(name string) bool {
	count := strings.Count(name, "A")
	count += strings.Count(name, "E")
	count += strings.Count(name, "I")
	if count >= 3 {
		return true
	}
	return false

}
func askString(msg string) string {

	// declara a var input
	var input string

	// apresenta a string Nome on-the-face
	fmt.Print(msg)

	// lê o que o usuário digitou
	fmt.Scanln(&input)
	input = strings.ToUpper(input)
	return input
}

func checkString(s string, chars string) bool {
	return !strings.ContainsAny(s, chars)
}

func validadeWeapon(x string) bool {
	y := []string{"FACA", "CORDA", "CALICE"}
	for _, i := range y {
		if i == x {
			return true
		}
	}
	return false
}
